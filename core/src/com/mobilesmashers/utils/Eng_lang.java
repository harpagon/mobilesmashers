package com.mobilesmashers.utils;

/*
 * Temporary solution.
 */
public final class Eng_lang {

	/* Info screen */
	public static final String
			INFO_CATCH_KEY = "To join balls, you have to shoot hook in ball by touch.",
			INFO_GOAL_KEY = "To win each round, you have to join every two balls of same type.",
			INFO_PLAYERS_MOVE_KEY = "You can move your player by tilting the phone.";

	/* Authors screen */
	public static final String
			INFO_AUTHORS_KEY = "Authors:",
			INFO_FIRSTAUTHOR_KEY = "Michał Kloc,",
			INFO_SECONDAUTHOR_KEY = "Kamil Kryus.";

	private Eng_lang() {
	}
}
